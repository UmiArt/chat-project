import {useState, useEffect} from "react";
import axios from "axios";
import Messages from "./Components/GetMessage/Messages";
import Post from "./Components/PostMessage/Post";

const url = 'http://146.185.154.90:8000/messages';

function App() {

    const [messages, setMessages] = useState([{
        message:'',
        author: '',
        datetime: '',
        _id: ''
    }]);

    useEffect(() => {

        const axiosData = async () => {

            try {
                const response = await axios.get(url);
                const mess = response.data
                setMessages([...mess]);
            } catch (e) {
                alert('Error : '+ e.response.status )
            }

        };
        axiosData().catch(e => console.error(e));

        setInterval(async () => {

            const newMess = await axios.get(url + '?datetime=' + messages[messages.length - 1]['datetime'])

            const newPost = newMess.data

            if (newMess.newPost !== 0) {

                setMessages([...newPost])
            }

        },3000);
    }, []);


    const getMessages = messages.map(newMessage =>

        <Messages key={newMessage.id}
                  message={newMessage.message}
                  author={newMessage.author}
                  datetime={newMessage.datetime}
                  id={newMessage._id}
        />
    );


  return (
    <div className="App">
        <div className="top"/>
        <div className="postMess">
            <Post/>
        </div>
        <div className="getMess">
            {getMessages}
        </div>
    </div>
  );
}

export default App;
