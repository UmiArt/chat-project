import React, {useState} from 'react';
import axios from "axios";
import './Post.css';

const url = 'http://146.185.154.90:8000/messages';

const Post = () => {

    const [post, setPost] = useState( [{
        message:'',
        author: '',
        _id:'',
        name: ''
    }]);

    const handle = (e) => {
        const newPost = {...post}
        newPost[e.target.name] = e.target.value
        setPost(newPost)
    }

    const submitForm = (e) => {
        e.preventDefault()
        const data = new URLSearchParams();
        data.set('message', post.message);
        data.set('author', post.author);
        axios.post(url, data);
        setPost('')
    }

    return (
        <div className='postDiv'>
            <form className="form" onSubmit={(e) => submitForm(e)}>
                <input type="text"
                       name="message"
                       placeholder="Enter a message"
                       value={post.message}
                       onChange={(e) => handle(e)}
                />
                <input type="text"
                       name="author"
                       placeholder="Enter your name"
                       value={post.author}
                       onChange={(e) => handle(e)}
                />
                <button className='btn' type="submit">Send</button>
            </form>

        </div>
    );
};

export default Post;