import React from 'react';
import './Messages.css';


const Messages = props => {
    return (
        <div className="MessBox">
            <p><strong>Message :</strong> {props.message}</p>
            <p><strong>Author :</strong> {props.author}</p>
            <p><strong>Datetime :</strong> {props.datetime}</p>
            <p><strong>Id :</strong> {props.id}</p>
        </div>
    );
};

export default Messages;